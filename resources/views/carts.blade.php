@extends('layouts.master')

@push('css')
<style>
    .img-fluid{
        max-height: 256px;
    }
</style>
@endpush

@section('content')
    @livewire('carts')
@endsection

@push('scripts')
@endpush
