<div>
    <div class="d-flex">
        <a class="btn btn-outline-dark" href="{{url('/carts')}}">
            <i class="bi-cart-fill me-1"></i>
            Cart
            <span class="badge bg-dark text-white ms-1 rounded-pill">{{$cartsCount}}</span>
        </a>
    </div>

</div>
