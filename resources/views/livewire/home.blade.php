<div>
    <div class="card" style="height: 32rem;">
        <img class="card-img-top" src="{{asset('assets/'.$image)}}" height="300px"
            alt="..." />
        <div class="card-body p-4">
            <div class="text-center">
                <h5 class="fw-bolder">{{$name}}</h5>
                RM {{$price}}.00
            </div>
        </div>
        <!-- Product actions-->
        <div
            class="card-footer p-4 pt-0 border-top-0 bg-transparent d-flex justify-content-center align-items-center">
            <div class="text-center me-2"><a class="btn btn-outline-dark mt-auto" href="{{url('/shopitem/'.$product_id)}}">View</a></div>
            <div class="text-center"><button class="btn btn-outline-dark mt-auto" wire:click='addtocart({{$product_id}})'>Add to cart</button></div>
        </div>
    </div>
</div>
