<div>
    <section class="py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-start mb-2">
                <a class="btn btn-outline-secondary" href="{{url('/')}}">Back</a>
            </div>
            @if (session()->has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
            <div class="row gx-4 gx-lg-5 align-items-center">
                <div class="col-md-6"><img class="card-img-top mb-5 mb-md-0" src="{{asset('assets/'.$product->image)}}" alt="..." height="700px" /></div>
                <div class="col-md-6">
                    <h1 class="display-5 fw-bolder">{{$product->name}}</h1>
                    <div class="fs-5 mb-5">
                        <span>RM{{$product->price}}/Item</span>
                    </div>
                    <p class="lead">{!!$product->description!!}</p>
                    <div class="d-flex mb-3">
                        @foreach ($product->sizes as $key =>$size)
                            <div class="form-check me-3">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" wire:model='size' id="flexRadioDefault{{$key}}" value="{{$size->name}}">
                                <label class="form-check-label" for="flexRadioDefault{{$key}}">{{$size->name}}</label>
                          </div>
                        @endforeach
                    </div>
                    <div class="d-flex">
                        <div class="d-flex justify-content-center align-items-center me-3">
                            <button class="btn btn-outline-secondary" wire:click='increment'>+</button>
                            <input class="form-control text-center" type="text" value="{{$quantity}}" style="max-width: 3rem;" />
                            <button class="btn btn-outline-secondary" wire:click='decrement'>-</button>
                        </div>
                        <button class="btn btn-outline-dark flex-shrink-0" type="button" wire:click='addToCart'>
                            <i class="bi-cart-fill me-1"></i>
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
