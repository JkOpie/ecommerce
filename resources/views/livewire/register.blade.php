<div>
    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Register</h1>
            </div>
            <div class="card bg-dark">
                <div class="card-body">
                    <div class="mb-3">
                        <label for="" class="form-label text-white">Name</label>
                        <input type="text" class="form-control" wire:model='name'>
                        @error('name')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label text-white">Email</label>
                        <input type="email" class="form-control" wire:model='email'>
                        @error('email')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label text-white">Password</label>
                        <input type="password" class="form-control" wire:model='password'>
                        @error('password')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label text-white">Confirm Password</label>
                        <input type="password" class="form-control" wire:model='confirm_password'>
                        @error('confirm_password')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="text-center pt-3">
                        <button class="btn btn-secondary w-100 btn-lg" wire:click='register'>Register</button>
                    </div>

                </div>
            </div>
        </div>
    </header>
</div>
