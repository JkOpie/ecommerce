<div>
    <section class="py-5">
        <div class="container px-4 px-lg-5 my-5">
            @if (session()->has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row">
                @if(!$carts)
                    <div class="col-md-12">
                        <p>No Item..</p>
                    </div>
                @endif

                @foreach ($carts as $cart)
                    <div class="col-md-6">
                        @livewire('cart-item', ['cart' => $cart], key($cart->id))
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
