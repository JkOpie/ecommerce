<div>
    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Login</h1>
            </div>
            <div class="card bg-dark">
                <div class="card-body">
                    @isset($error_message)
                        <div class="alert alert-danger my-3">{{ $error_message }}</div>
                    @endisset

                    <div class="mb-3">
                        <label for="" class="form-label text-white">Email</label>
                        <input type="text" class="form-control" wire:model='email'>
                        @error('email')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label text-white">Password</label>
                        <input type="password" class="form-control" wire:model='password'>
                        @error('password')
                            <i><span class="text-danger">{{$message}}</span></i>
                        @enderror
                    </div>

                    <div class="text-center pt-3">
                        <button class="btn btn-secondary w-100 btn-lg" wire:click='save'>Login</button>
                    </div>

                </div>
            </div>
        </div>
    </header>
</div>
