<div wire:ignore>
    <div class="card mb-3" >
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/assets/' . $product_image) }}" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title fw-bold">{{ $product_name }}</h5>
                    <p class="card-text">{{ $product_description }}</p>
                    <p class="card-text fw-bold">RM{{ $product_price }}.00</p>

                    <div class="d-flex aling-items-center">
                        <button class="btn btn-outline-secondary" wire:click='increment'>+</button>
                        <input class="form-control text-center" type="text" wire:model='quantity' style="max-width: 3rem;" />
                        <button class="btn btn-outline-secondary me-2" wire:click='decrement'>-</button>
                        <button class="btn btn-outline-danger" wire:click='cartdelete'>Delete</button>
                    </div>
                    <h6 class="fw-bolder mt-3">Total Price :</h6>
                    <div class="d-flex align-items-center">
                        <span class="fw-bold" style="width: 3rem;"> RM : </span><input type="text" class="form-control" wire:model='total_price' disabled></span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
