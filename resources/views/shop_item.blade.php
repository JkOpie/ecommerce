@extends('layouts.master')

@push('css')
@endpush

@section('content')
    @livewire('product-item')
@endsection

@push('scripts')
@endpush
