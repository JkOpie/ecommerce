<!DOCTYPE html>
<html lang="en">
    @include('layouts.head')
    @stack('css')

    <body>
        <!-- Navigation-->
        @include('layouts.nav')
        <!-- Header-->
        <!-- Section-->
        @yield('content')
        <!-- Footer-->
        @include('layouts.footer')
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        @livewireScripts
        @stack('scripts')
    </body>
</html>
