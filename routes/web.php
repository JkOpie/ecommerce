<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ProductController::class, 'index']);
Route::get('/shopitem/{id}', [ProductController::class, 'show']);

Route::get('/login', [AuthController::class, 'getLogin']);
Route::get('/register', [AuthController::class, 'getRegister']);

Route::get('/carts', [CartController::class, 'index']);
