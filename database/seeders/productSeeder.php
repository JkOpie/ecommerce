<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductSize;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "name" => 'Casual Classic Patchwork Jacket - Pink / L',
                "description" => "Kclot | Tops Collection | Designed for maximum comfort and durability with styles ranging from basic colors, retro prints, and detailed embroidery",
                "price" => "30",
                "discount_price" => null,
                'image' => 'product1.png',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => 'Casual Outfits',
                "description" => null,
                "price" => "35",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product2.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => 'Green Bomber Jacket Mens, Best Faux Leather Jacket For Men',
                "description" => "Shop leather jackets for men online at affordable prices on NewChic, choose from green bomber jacket, faux leather jacket and plus size bomber jacket mens.",
                "price" => "50",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product3.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => 'Mode Outfits',
                "description" => null,
                "price" => "60",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product4.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => 'Streetwear',
                "description" => null,
                "price" => "45",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product5.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => 'Retro Outfits',
                "description" => null,
                "price" => "45",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product6.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => "Men's Hoodies",
                "description" => "Vintage big and tall hoodies mens at discount cheap prices, and more popular zip up hoodies and xxxxl plus size funny colorful embroidered pullover hooodie online",
                "price" => "55",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product7.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],
            [
                "name" => "Herren Sweatshirt Cord Patchwork Vintage Drop Pocket Hoodie",
                "description" => null,
                "price" => "55",
                "quantity" => "10",
                "discount_price" => null,
                'image' => 'product8.jpg',
                "sizes" => [
                    'XS' ,
                    'S' ,
                    'M' ,
                    'L' ,
                    'XL' ,
                ],
            ],

        ];

        foreach ($data as $key => $value) {
            $product = Product::firstOrCreate([
                "name" => $value['name'],
                "description" => $value['description'],
                "price" => $value['price'],
                "discount_price" => $value['discount_price'],
                "image" => $value['image']
            ]);

            foreach ($value['sizes'] as $key => $item) {
                ProductSize::firstOrCreate(['name' => $item, 'product_id' => $product->id]);
            }
        }

    }
}
