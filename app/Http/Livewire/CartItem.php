<?php

namespace App\Http\Livewire;

use App\Models\Carts;
use Livewire\Component;

class CartItem extends Component
{
    public $cartid, $product_name,$product_image, $product_description, $quantity, $total_price, $product_price;

    protected $listeners = ['$refresh'];

    public function mount($cart)
    {
        $this->cartid = $cart->id;
        $this->product_image = $cart->product->image;
        $this->product_name = $cart->product->name;
        $this->product_description = $cart->product->description;
        $this->product_price = $cart->product->price;
        $this->quantity = $cart->quantity;
        $this->total_price = $cart->total_price;
    }

    public function dehydrateQuantity($value)
    {
        $this->total_price = number_format(((double) $this->product_price) * ((int) $value), 2, '.', '');

        Carts::where('id', $this->cartid)->update([
            'quantity' => $value,
            'total_price' => number_format(((double) $this->product_price) * ((int) $value), 2, '.', ''),
        ]);
    }

    public function increment()
    {
        $this->quantity++;

    }

    public function decrement()
    {
        if( $this->quantity >= 2){
            $this->quantity--;
        }
    }


    public function cartdelete()
    {
        Carts::where('id', $this->cartid)->delete();
        $this->emitTo('cart-count','refreshCount');
        $this->emitTo('carts', 'deletedItem');
    }

    public function render()
    {
        return view('livewire.cart-item');
    }
}
