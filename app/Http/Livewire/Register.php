<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Register extends Component
{
    public $name, $email,$password,$confirm_password;

    protected $rules = [
        'email' => 'required|email|unique:users,email',
        'password' => 'required',
        'confirm_password' => 'required|same:password',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function register()
    {
        $this->validate();

        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password)
        ]);

        Auth::loginUsingId($user->id);

        return redirect('/');
    }

    public function render()
    {
        return view('livewire.register');
    }
}
