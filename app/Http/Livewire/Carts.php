<?php

namespace App\Http\Livewire;

use App\Models\Carts as ModelsCarts;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Carts extends Component
{
    public $carts;
    protected $listeners = ['deletedItem', '$refresh'];

    public function mount()
    {
        $this->carts = ModelsCarts::all();
    }

    public function render()
    {

        return view('livewire.carts');
    }

    public function deletedItem()
    {
        $this->emit('$refresh');
    }
}
