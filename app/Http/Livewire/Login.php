<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $email,$password,$error_message;

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function save()
    {
        if(Auth::attempt(['email' => $this->email, 'password' => $this->password])){
            return redirect('/');
        }

        $this->error_message = 'Invalid Email or password, Please try again!.';
    }

    public function render()
    {
        return view('livewire.login');
    }
}
