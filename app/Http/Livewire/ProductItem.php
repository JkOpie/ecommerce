<?php

namespace App\Http\Livewire;

use App\Models\Carts;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ProductItem extends Component
{
    public $product, $quantity = 1, $size = 'XS';

    public function mount(Request $request)
    {
        $this->product = Product::where('id', $request->id)->with('sizes')->firstOrFail();
    }

    public function increment()
    {
        $this->quantity++;
    }

    public function decrement()
    {
        if( $this->quantity >= 2){
            $this->quantity--;
        }
    }

    public function addToCart()
    {
        if(!Auth::check()){
            return redirect('/login');
        }

        $cart = Carts::where('product_id',$this->product->id)->first();

        if($cart){
            $quantity = $cart->quantity + $this->quantity;
            $total_price = number_format(((double) $this->product->price) * ((int)$quantity), 2, '.', '');
            $cart->update(['quantity'=>$quantity, 'total_price'=>$total_price]);
        }else{
            Carts::create([
                'user_id' => Auth::user()->id,
                'product_id' => $this->product->id,
                'quantity' => $this->quantity,
                'size' => $this->size,
                'total_price' => number_format(((double) $this->product->price) * ((int)$this->quantity), 2, '.', '')
            ]);
        }

        $this->emitTo('cart-count','refreshCount');
        session()->flash('message', 'Carts Updated.');
    }

    public function render()
    {
        return view('livewire.product-item');
    }
}
