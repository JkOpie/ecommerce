<?php

namespace App\Http\Livewire;

use App\Models\Carts;
use Livewire\Component;

class Home extends Component
{
    public $product_id, $name, $description, $price, $image;

    public function mount($product)
    {
        $this->product_id = $product->id;
        $this->name = $product->name;
        $this->description = $product->description;
        $this->price = $product->price;
        $this->image = $product->image;
    }

    public function addtocart($id)
    {
        $cart = Carts::where('product_id',$id)->first();
        $quantity = 1;
        if($cart){
            $quantity = $cart->quantity + $quantity;
            $total_price = number_format(((double) $this->price) * ((int)$quantity), 2, '.', '');
            $cart->update([
                'quantity'=>$quantity,
                'total_price'=>$total_price
            ]);

        }else{
            Carts::create([
                'product_id' => $this->product_id,
                'quantity' => $quantity,
                'size' => 'XS',
                'total_price' => number_format(((double) $this->price) * ((int)$quantity), 2, '.', '')
            ]);
        }

        $this->emitTo('cart-count','refreshCount');
    }

    public function render()
    {
        return view('livewire.home');
    }
}
