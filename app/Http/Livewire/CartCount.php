<?php

namespace App\Http\Livewire;

use App\Models\Carts;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CartCount extends Component
{
    public $cartsCount = 0;

    protected $listeners = ['refreshCount'];

    public function refreshCount()
    {
        $this->cartsCount = Carts::count();
    }

    public function mount()
    {
        $this->cartsCount = Carts::count();
    }

    public function render()
    {
        return view('livewire.cart-count');
    }
}
