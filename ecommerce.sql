-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 04, 2023 at 02:20 PM
-- Server version: 8.0.33-0ubuntu0.20.04.2
-- PHP Version: 8.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `product_id`, `quantity`, `size`, `total_price`, `created_at`, `updated_at`) VALUES
(9, NULL, 1, '1', 'XS', '30.00', '2023-08-04 12:09:56', '2023-08-04 14:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2014_10_12_000000_create_users_table', 1),
(22, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(25, '2023_08_03_023146_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`, `price`, `discount_price`, `created_at`, `updated_at`) VALUES
(1, 'Casual Classic Patchwork Jacket - Pink / L', 'Kclot | Tops Collection | Designed for maximum comfort and durability with styles ranging from basic colors, retro prints, and detailed embroidery', 'product1.png', '30', NULL, '2023-08-03 07:40:10', '2023-08-03 07:40:10'),
(2, 'Casual Outfits', NULL, 'product2.jpg', '35', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(3, 'Green Bomber Jacket Mens, Best Faux Leather Jacket For Men', 'Shop leather jackets for men online at affordable prices on NewChic, choose from green bomber jacket, faux leather jacket and plus size bomber jacket mens.', 'product3.jpg', '50', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(4, 'Mode Outfits', NULL, 'product4.jpg', '60', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(5, 'Streetwear', NULL, 'product5.jpg', '45', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(6, 'Retro Outfits', NULL, 'product6.jpg', '45', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(7, 'Men\'s Hoodies', 'Vintage big and tall hoodies mens at discount cheap prices, and more popular zip up hoodies and xxxxl plus size funny colorful embroidered pullover hooodie online', 'product7.jpg', '55', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(8, 'Herren Sweatshirt Cord Patchwork Vintage Drop Pocket Hoodie', NULL, 'product8.jpg', '55', NULL, '2023-08-03 07:42:29', '2023-08-03 07:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `product_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(2, 1, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(3, 1, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(4, 1, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(5, 1, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(6, 2, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(7, 2, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(8, 2, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(9, 2, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(10, 2, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(11, 3, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(12, 3, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(13, 3, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(14, 3, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(15, 3, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(16, 4, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(17, 4, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(18, 4, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(19, 4, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(20, 4, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(21, 5, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(22, 5, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(23, 5, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(24, 5, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(25, 5, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(26, 6, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(27, 6, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(28, 6, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(29, 6, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(30, 6, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(31, 7, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(32, 7, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(33, 7, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(34, 7, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(35, 7, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(36, 8, 'XS', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(37, 8, 'S', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(38, 8, 'M', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(39, 8, 'L', '2023-08-03 07:42:29', '2023-08-03 07:42:29'),
(40, 8, 'XL', '2023-08-03 07:42:29', '2023-08-03 07:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Test', 'test@gmail.com', NULL, '$2y$10$FJvR79tSg0oOWi6E29OhP.zGMHzIWs4poHrgIAxYAHC0q2xB235DO', NULL, '2023-08-03 09:33:06', '2023-08-03 09:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_products`
--

CREATE TABLE `user_products` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_products`
--
ALTER TABLE `user_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_products`
--
ALTER TABLE `user_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
